using System;

namespace SRP_1_1
{
    public class ProcessingCenterGateway
    {
        public void Charge(decimal ticketPrice, PaymentDetails payment)
        {
            Console.WriteLine($"Processing payment of {ticketPrice} with {payment.Method.ToString()}");
        }
    }
}