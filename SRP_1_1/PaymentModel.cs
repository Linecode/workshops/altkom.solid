using System;

namespace SRP_1_1
{
    public class PaymentModel
    {
        private decimal _cashAccepted;

        public void BuyTicket(TicketDetails ticket, PaymentDetails payment, Action onPayChangeToMobilePhone)
        {
            if (payment.Method == PaymentMethod.CreditCard)
            {
                ChargeCard(ticket, payment);
            }
            else
            {
                AcceptCash(ticket);
                DispenseChange(ticket, onPayChangeToMobilePhone);
            }
        }

        private void DispenseChange(TicketDetails ticket, Action onPayChangeToMobilePhone)
        {
            if (_cashAccepted > ticket.Price && !TryToDispense(_cashAccepted - ticket.Price))
                onPayChangeToMobilePhone.Invoke();
            
            Console.WriteLine("Cash Dispensed");
        }

        private bool TryToDispense(decimal cashAccepted)
        {
            // just a stub
            return false;
        }

        private void AcceptCash(TicketDetails ticket)
        {
            var r = new Random();
            _cashAccepted = r.Next((int)ticket.Price, (int)ticket.Price + 1000);
            Console.WriteLine($"Cash accepted: {_cashAccepted}");
        }

        private void ChargeCard(TicketDetails ticket, PaymentDetails payment)
        {
            var gateway = new ProcessingCenterGateway();
            gateway.Charge(ticket.Price, payment);
        }
    }
}