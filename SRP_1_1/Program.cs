﻿using System;

namespace SRP_1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var ticket = new TicketDetails
            {
                Price = 10m,
                Destination = "London"
            };
            
            #region CreditCard
            var paymentDetails = new PaymentDetails
            {
                Method = PaymentMethod.CreditCard
            };
            var paymentModel = new PaymentModel();
            paymentModel.BuyTicket(ticket, paymentDetails, () => {});
            #endregion
            
            // Buy with cash
            #region Cash

            // var paymentDetails = new PaymentDetails
            // {
            //     Method = PaymentMethod.Cash
            // };
            // var paymentModel = new PaymentModel();
            // paymentModel.BuyTicket(ticket, paymentDetails, () =>
            // {
            //     Console.WriteLine("Send Phone Notification");
            // });

            #endregion
        }
    }
}