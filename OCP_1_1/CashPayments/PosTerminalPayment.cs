using System;

namespace OCP_1_1.CashPayments
{
    public class PosTerminalPayment : PaymentModel, ICashPayment
    {
        private readonly Action _onPayChangeToMobilePhone;
        private decimal _cashAccepted;

        public PosTerminalPayment(TicketDetails ticketDetails, Action onPayChangeToMobilePhone) : base(ticketDetails)
        {
            _onPayChangeToMobilePhone = onPayChangeToMobilePhone;
        }

        public override void BuyTicket()
        {
            AcceptCash();
            DispenseChange();
        }

        public void AcceptCash()
        {
            var r = new Random();
            _cashAccepted = r.Next((int)_ticketDetails.Price, (int)_ticketDetails.Price * 1000);
            Console.WriteLine($"Cash accepted: {_cashAccepted}");
        }

        public void DispenseChange()
        {
            if (_cashAccepted > _ticketDetails.Price && !TryToDispense())
            {
                _onPayChangeToMobilePhone?.Invoke();
            }
            
            Console.WriteLine("Cash Dispensed");
        }

        private bool TryToDispense()
        {
            // just a stub 
            return false;
        }
    }
}