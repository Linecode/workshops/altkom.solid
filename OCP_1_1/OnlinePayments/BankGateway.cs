using System;

namespace OCP_1_1.OnlinePayments
{
    public class BankGateway : ICreditCardPayment
    {
        public void ChargeCard(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            Console.WriteLine($"Charged {ticketDetails.Price} with bank gateway");
        }
    }
}