namespace OCP_1_1.OnlinePayments
{
    public interface ICreditCardPayment
    {
        void ChargeCard(TicketDetails ticketDetails, PaymentDetails paymentDetails);
    }
}