namespace OCP_1_1
{
    public class TicketDetails
    {
        public decimal Price { get; set; }
        public string Destination { get; set; }
    }
}