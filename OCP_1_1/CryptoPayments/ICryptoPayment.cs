using System;

namespace OCP_1_1.CryptoPayments
{
    public interface ICryptoPayment
    {
        public Guid ExchangeId { get; }
        decimal ExchangeRate { get; }
        void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails);
    }
}