using System;
using System.Collections.Generic;
using System.Linq;

namespace OCP_1_1.CryptoPayments
{
    public class CryptoPayment : PaymentModel
    {
        private readonly PaymentDetails _payment;
        private readonly UserContext _userContext;

        private readonly ICollection<ICryptoPayment> _exchanges = new List<ICryptoPayment>
        {
            new FirstCryptoPaymentPayment(),
            new SecondCryptoPaymentPayment()
        };

        public CryptoPayment(TicketDetails ticketDetails, PaymentDetails payment, UserContext userContext) : base(ticketDetails)
        {
            _payment = payment;
            _userContext = userContext;
        }

        public override void BuyTicket()
        {
            ICryptoPayment cryptoPaymentPayment;

            if (_userContext.PreferredCryptoExchange.Equals("Smallest"))
                cryptoPaymentPayment = _exchanges.OrderBy(x => x.ExchangeRate).First();
            else
                cryptoPaymentPayment = _exchanges.Single(x => x.ExchangeId == Guid.Parse(_userContext.PreferredCryptoExchange));

            cryptoPaymentPayment.Charge(base._ticketDetails, _payment);
        }
    }
}