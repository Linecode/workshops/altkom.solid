using System;

namespace OCP_1_1.CryptoPayments
{
    public class FirstCryptoPaymentPayment : ICryptoPayment
    {
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            Console.WriteLine($"Charged {ticketDetails.Price} with first crypto gateway");
        }

        public Guid ExchangeId => new ("C5422FD3-B528-496B-B6C6-0881E734CFAA");
        public decimal ExchangeRate => 1.15m;
    }

    public class SecondCryptoPaymentPayment : ICryptoPayment
    {
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            Console.WriteLine($"Charged {ticketDetails.Price} with second crypto gateway");
        }

        public Guid ExchangeId => new ("3CA5E284-2859-47FD-BB04-0AB3942BFFFC");
        public decimal ExchangeRate => 1.17m;
    }
}