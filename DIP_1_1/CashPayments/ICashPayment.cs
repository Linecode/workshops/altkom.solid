namespace DIP_1_1.CashPayments
{
    public interface ICashPayment
    {
        void AcceptCash();
        void DispenseChange();
    }
}