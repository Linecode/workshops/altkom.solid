namespace DIP_1_1
{
    public class UserContext
    {
        public string PhoneNumber { get; set; }
        public bool PhoneNotificationEnabled { get; set; }
        public string PreferredCryptoExchange { get; set; }
    }
}