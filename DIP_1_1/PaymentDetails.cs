namespace DIP_1_1
{
    public class PaymentDetails
    {
        public PaymentMethod Method { get; set; } = PaymentMethod.Cash;
    }
}