using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP_1_1.CryptoPayments
{
    public static class Exchanges
    {
        private static readonly List<ICryptoPayment> _list = new ();

        public static IReadOnlyList<ICryptoPayment> All => _list.AsReadOnly();

        public static void RegisterCryptoExchange(ICryptoPayment exchange)
            => _list.Add(exchange);
    } 
    
    public class CryptoExchangeFinder
    {
        private readonly UserContext _userContext;

        public CryptoExchangeFinder(UserContext userContext)
        {
            _userContext = userContext;
        }

        public ICryptoPayment FindCryptoExchange()
        {
            ICryptoPayment cryptoPaymentExchange;

            if (_userContext.PreferredCryptoExchange.Equals("Smallest"))
                cryptoPaymentExchange = Exchanges.All.OrderBy(x => x.ExchangeRate).First();
            else
                cryptoPaymentExchange =
                    Exchanges.All.Single(x => x.ExchangeId == Guid.Parse(_userContext.PreferredCryptoExchange));

            return cryptoPaymentExchange;
        }
    }
}