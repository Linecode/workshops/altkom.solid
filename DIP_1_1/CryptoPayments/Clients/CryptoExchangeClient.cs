using System;

namespace DIP_1_1.CryptoPayments.Clients
{ 
    public interface IInternalCryptoClient
    {
        void Charge(decimal amount);
    }

    public interface IExternalCryptoClient : IInternalCryptoClient
    {
        decimal GetGasFee();
    }
    
    public class FirstCryptoExchangeClient : IExternalCryptoClient
    {
        public decimal GetGasFee()
        {
            return 0.30m;
        }

        public void Charge(decimal amount)
        {
            Console.WriteLine($"Charged {amount} with first crypto gateway");
        }
    }

    public class SecondCryptoExchangeClient : IExternalCryptoClient
    {
        public decimal GetGasFee()
        {
            return 0.25m;
        }

        public void Charge(decimal amount)
        {
            Console.WriteLine($"Charged {amount} with second crypto gateway");
        }
    }

    public class InternalCryptoExchangeClient : IInternalCryptoClient
    {
        public decimal GetGasFee()
        {
            throw new NotImplementedException();
        }

        public void Charge(decimal amount)
        {
            Console.WriteLine($"Charged {amount} with internal crypto gateway");
        }
    }
}