namespace DIP_1_1.CryptoPayments
{
    public class CryptoPayment : PaymentModel
    {
        private readonly PaymentDetails _payment;
        private readonly CryptoExchangeFinder _cryptoExchangeFinder;

        public CryptoPayment(TicketDetails ticketDetails, PaymentDetails payment, UserContext userContext) : base(ticketDetails)
        {
            _payment = payment;
            _cryptoExchangeFinder = new CryptoExchangeFinder(userContext);
        }

        public override void BuyTicket()
        {
            var cryptoExchange = _cryptoExchangeFinder.FindCryptoExchange(); 
            cryptoExchange?.Charge(base._ticketDetails, _payment);
        }
    }
}