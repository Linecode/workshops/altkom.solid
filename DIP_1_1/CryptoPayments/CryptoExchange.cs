using System;
using DIP_1_1.CryptoPayments.Clients;

namespace DIP_1_1.CryptoPayments
{
    public class FirstCryptoPaymentExchange : ICryptoPayment
    {
        private readonly IExternalCryptoClient _client = new FirstCryptoExchangeClient();
        
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            if (_client.GetGasFee() < 0.5m)
                _client.Charge(ticketDetails.Price);
        }

        public Guid ExchangeId => new ("C5422FD3-B528-496B-B6C6-0881E734CFAA");
        public decimal ExchangeRate => 1.15m;
    }

    public class SecondCryptoPaymentExchange : ICryptoPayment
    {
        private readonly IExternalCryptoClient _client = new SecondCryptoExchangeClient();
        
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            if (_client.GetGasFee() < 0.5m)
                _client.Charge(ticketDetails.Price);
        }
        
        public Guid ExchangeId => new ("3CA5E284-2859-47FD-BB04-0AB3942BFFFC");

        public decimal ExchangeRate => 1.17m;
    }

    public class InternalCryptoPaymentExchange : ICryptoPayment
    {
        private readonly IInternalCryptoClient _client = new InternalCryptoExchangeClient();
        
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            _client.Charge(ticketDetails.Price);
        }
        
        public Guid ExchangeId => new ("41290167-3555-4E58-90AC-E7C8841A677F");

        public decimal ExchangeRate => 1.13m;
    }
}