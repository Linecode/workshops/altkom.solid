﻿using System;

namespace LSP_1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var rect = new Rectangle() { Width = 2, Height = 5 };
            var rectArea = AreaCalculator.CalRectangle(rect);

            Console.WriteLine($"Rectangle Area = {rectArea}");

            var square = new Square() { Height = 7 };
            var squareArea = AreaCalculator.CalcSquare(square);

            Console.WriteLine($"Square area = {squareArea}");
        }
    }

    public class Rectangle
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class Square : Rectangle
    {
    }

    public class AreaCalculator
    {
        public static int CalcSquare(Square square) => square.Height * square.Height;
        public static int CalRectangle(Rectangle rect) => rect.Height * rect.Width;

        public static int CalcArea(Rectangle rect)
        {
            if (rect is Square)
                return rect.Height * rect.Height;
            else
                return rect.Height * rect.Width;
        }
    }
}