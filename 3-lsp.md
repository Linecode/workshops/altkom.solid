# Liskov Substitution Principle

## Zadanie 1.1

- Poświęć chwilę aby przeanalizować kod znajdujący się w pliku Program.cs. Czy widzisz jakieś problemy z kodem?
- Stwórz interfejs `IShape` i zadeklaruj w nim metodę `int CalculateArea`
- Następnie utwórz klasy `Square` i `Rectangle`, które będą implementowały interfejs `IShape`
- W klasie `Square` dodaj propertis `SquareLength`
- W klasie `Rectangle` dodaj propertisy `Width` i `Height`
- Dodaj implementacje method `CalculateArea` w obu klasach