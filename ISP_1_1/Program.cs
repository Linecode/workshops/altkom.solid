﻿using System;
using ISP_1_1.CashPayments;
using ISP_1_1.CryptoPayments;
using ISP_1_1.OnlinePayments;

namespace ISP_1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Application configuration
            Exchanges.RegisterCryptoExchange(new FirstCryptoPaymentExchange());
            Exchanges.RegisterCryptoExchange(new SecondCryptoPaymentExchange());
            Exchanges.RegisterCryptoExchange(new InternalCryptoPaymentExchange());
            
            // Usage
            var ticket = new TicketDetails
            {
                Price = 10m,
                Destination = "London"
            };
            var paymentDetails = new PaymentDetails
            {
                Method = PaymentMethod.Crypto
            };
            var userContext = new UserContext
            {
                PhoneNumber = "",
                PhoneNotificationEnabled = false,
                PreferredCryptoExchange = "Smallest"
            };
            PaymentModel payment = GetPaymentOption(paymentDetails, ticket, userContext);
            payment.BuyTicket();
        }

        private static PaymentModel GetPaymentOption(PaymentDetails paymentDetails, TicketDetails ticket, UserContext userContext)
        {
            if (paymentDetails.Method == PaymentMethod.Cash)
                return new PosTerminalPayment(ticket, userContext.PhoneNotificationEnabled ? () =>
                {
                    Console.WriteLine($"Send Phone Notification to: {userContext.PhoneNumber}");
                } : () => { });
            
            if (paymentDetails.Method == PaymentMethod.Crypto)
                return new CryptoPayment(ticket, paymentDetails, userContext);
            
            if (paymentDetails.Method == PaymentMethod.CreditCard)
                return new OnlinePayment(ticket, paymentDetails);

            throw new NotSupportedException();
        }
    }
}