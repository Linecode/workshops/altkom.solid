using System;
using ISP_1_1.CryptoPayments.Clients;

namespace ISP_1_1.CryptoPayments
{
    public class FirstCryptoPaymentExchange : ICryptoPayment
    {
        private readonly FirstCryptoExchangeClient _client = new ();
        
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            if (_client.GetGasFee() < 0.5m)
                _client.Charge(ticketDetails.Price);
        }

        public Guid ExchangeId => new ("C5422FD3-B528-496B-B6C6-0881E734CFAA");
        public decimal ExchangeRate => 1.15m;
    }

    public class SecondCryptoPaymentExchange : ICryptoPayment
    {
        private readonly SecondCryptoExchangeClient _client = new ();
        
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            if (_client.GetGasFee() < 0.5m)
                _client.Charge(ticketDetails.Price);
        }
        
        public Guid ExchangeId => new ("3CA5E284-2859-47FD-BB04-0AB3942BFFFC");

        public decimal ExchangeRate => 1.17m;
    }

    public class InternalCryptoPaymentExchange : ICryptoPayment
    {
        private readonly InternalCryptoExchangeClient _client = new ();
        
        public void Charge(TicketDetails ticketDetails, PaymentDetails paymentDetails)
        {
            _client.Charge(ticketDetails.Price);
        }
        
        public Guid ExchangeId => new ("41290167-3555-4E58-90AC-E7C8841A677F");

        public decimal ExchangeRate => 1.13m;
    }
}