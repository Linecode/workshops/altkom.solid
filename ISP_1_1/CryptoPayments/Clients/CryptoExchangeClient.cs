using System;

namespace ISP_1_1.CryptoPayments.Clients
{
    public interface ICryptoExchangeClient
    {
        decimal GetGasFee();
        void Charge(decimal amount);
    }
    
    public class FirstCryptoExchangeClient : ICryptoExchangeClient
    {
        public decimal GetGasFee()
        {
            return 0.30m;
        }

        public void Charge(decimal amount)
        {
            Console.WriteLine($"Charged {amount} with first crypto gateway");
        }
    }

    public class SecondCryptoExchangeClient : ICryptoExchangeClient
    {
        public decimal GetGasFee()
        {
            return 0.25m;
        }

        public void Charge(decimal amount)
        {
            Console.WriteLine($"Charged {amount} with second crypto gateway");
        }
    }

    public class InternalCryptoExchangeClient : ICryptoExchangeClient
    {
        public decimal GetGasFee()
        {
            throw new NotImplementedException();
        }

        public void Charge(decimal amount)
        {
            Console.WriteLine($"Charged {amount} with internal crypto gateway");
        }
    }
}