namespace ISP_1_1
{
    public enum PaymentMethod
    {
        CreditCard,
        Cash,
        Crypto
    }
}