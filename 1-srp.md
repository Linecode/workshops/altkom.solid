# Single Responsibility Principle

## Zadanie 1.1

- Stwórz klasę abstrakcyjną `PaymentModel`, która przyjmuje jako parametr konstruktura instancję klasy `TicketDetails`
- W klasie `PaymentModel` utwórz metodę abstrakcyjną `BuyTicket`
- Stwórz interfejs o nazwię `ICashPayments`, który definiuje dwie metody
  - `void AcceptCash()`
  - `void DispenseChange()`
- Stwórz interfejs o nazwie `ICreditCardPayments`, który definiuje jedną metodę
  - `void ChargeCard` z parametrami `TicketDetails` i `PaymentDetails`
- Stwórz klasę `BankGateway`, która ma implementować interfejs `ICreditCardPayment`. Na Chwilę obecną ta klasa może tylko wyświetlać napis na konsoli że próbowano zapłacić za pomocą BankGateway.
- Stwórz klasę `OnlinePayment`, która dziedziczy po klasie `PaymentModel`.
- Nadpisz konstruktor klasy `OnlinePayment` tak aby pobierał dwa obiekty, ticketDetails i paymentDetails. Obiekt ticketDetails od razu możesz przekazać do konstruktora klasy bazowej za pomocą `: base(ticketDetails)`
- W konstruktorze klasy `OnlinePayment` utwórz instancję obiektu `BankGateway` i przypisz ja do prywatnego pola `_bankGateway`
- Stwórz klasę `TerminalPayments`, która ma dziedziczyć po klasie `PaymentModel` i implementować interfejs `ICashPayment`
- Nadpisz konstruktor klasy `TerminalPayment` tak aby akceptował paramtery ticketDetails ora Action onPayChangeToMobilePhone. Obiekt ticketDetails od razu możesz przekazać do konstruktora klasy bazowej za pomocą `: base(ticketDetails)`
- W klasie `TerminalPayments` nadpisz metodę `BuyTicket`. W ciele metody wywołaj metody, które musiałeś zaimplementować z interfejsu `ICashPayment` czyli `AcceptCash` i `DispenseChang`
- W ciele metody `AcceptCash` dodaj następujący kod: 
```csharp
var r = new Random();
_cashAccepted = r.Next((int)_ticketDetails.Price, (int)_ticketDetails.Price * 1000);
Console.WriteLine($"Cash accepted: {_cashAccepted}");
```
- Natomiast w ciele metody DispeneChange dodaj następujący kod:
```csharp
if (_cashAccepted > _ticketDetails.Price && !TryToDispense())
{
    _onPayChangeToMobilePhone?.Invoke();
}

Console.WriteLine("Cash Dispensed");
```
- Dodaj jeszcze metodę prywatną `TryToDispense` na chwile obecną może ona zwracać zahardkodowaną wartość `false`
- W pliku `Program.cs` dodaj prywatną statyczną metodę `GetPaymentOption`, która przyjmuje parametry paymentDetails i ticketDetails
- W ciele tej metody utwórz odpowiedni obiekt *Payment (TerminalPayment lub OnlinePayment) bazując na wartości pola paymentDetails.Method.
Dla terminal payment jako paramter onPayChangeToMobilePhone możesz użyć poniższego wyrażenia lambda
```csharp
() => {
    Console.WriteLine("Send Phone Notification");
}
```
- Dostosuj funkcję `Main` z klasy `Program` tak aby przetestować działanie programu.

## Zadanie 1.2 

- Utwórz statyczną klasę `DataFetcher` z statyczną metodą `Get`, która ma zwracać obiekt klasy `ReportData`
- Do metody `Get` przenieś kod odpowiedzialny za generowanie testowych danych
- Utwórz klasę abstrakcyjną o nazwie `Formatter` z metodą abstrakcyjną `string Format(ReportData data)`
- Utwórz oddzielnego klasy dla każdego formatera (JsonFormatter, TextFormatter, YmlFormatter)
- Dla każdego formatter-a zaimplementuj metodę Format zgodnie z poprzednią implementacją
- Zmień implementację klasy `ReportGenerator` tak aby metoda GetReport przyjmowała reportData i formatter jako parametry. Wywołaj metodę Format na obiekcie Formatter przekazując reportData jako parametr.
- Dostosuj metodę `Main` klasy Program do nowej składni
- Przetestuj program czy wszystko działa poprawnie