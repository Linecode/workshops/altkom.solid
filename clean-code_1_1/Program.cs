﻿using System;
using System.Collections.Generic;
using clean_code_1_1.InvoiceModule;

namespace clean_code_1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // user input
            #region userInput
            var @for = new InvoiceParty
            {
                Address = "dsadsadas",
                Email = "m@a.pl",
                Id = new Guid("0DB60AE3-7BA9-45FC-A789-A5105556C1F3"),
                Name = "Test Company",
                TaxNo = "CHL09876521234567890"
            };
            var lines = new List<InvoiceLine>
            {
                new()
                {
                    Id = 1,
                    Name = "Line 1",
                    Price = 10.3m,
                    Quantity = 3
                },
                new()
                {
                    Id = 1,
                    Name = "Line 1",
                    Price = 10.3m,
                    Quantity = 3
                }
            };
            #endregion

            var service = new InvoiceService(new InvoiceRepository());

            var invoice = service.Create(@for, InvoiceService.OurCompany, lines);
            
            if (invoice.IsValid())
                service.Save(invoice);
        }
    }
}