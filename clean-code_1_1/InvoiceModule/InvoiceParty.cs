using System;

namespace clean_code_1_1.InvoiceModule
{
    public class InvoiceParty
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string TaxNo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}