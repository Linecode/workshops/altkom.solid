namespace clean_code_1_1.InvoiceModule
{
    public class InvoiceLine
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}