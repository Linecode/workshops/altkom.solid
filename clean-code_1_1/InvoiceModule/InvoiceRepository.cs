using System;
using System.Collections.Generic;

namespace clean_code_1_1.InvoiceModule
{
    public class InvoiceRepository : List<InvoiceModel>
    {
        public new void Add(InvoiceModel model)
        {
            if (Count > 5)
            {
                throw new Exception("Reach limit of free tier");
            }
            
            base.Add(model);
        }

        public void SaveChanges()
        {
            // just to stub EF API
        }
    }
}