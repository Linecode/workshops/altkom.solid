using System;
using System.Collections.Generic;

namespace clean_code_1_1.InvoiceModule
{
    /// <summary>
    /// Invoice Service
    /// </summary>
    public class InvoiceService
    {
        private readonly InvoiceRepository _repository;

        public InvoiceService(InvoiceRepository repository)
        {
            _repository = repository;
        }
        
        /// <summary>
        /// Creates Invoice
        /// </summary>
        /// <param name="from">From who</param>
        /// <param name="for">For who</param>
        /// <param name="items">Invoice items</param>
        /// <param name="dateOfIssue">Issue Date</param>
        /// <param name="paymentDue">Payment due</param>
        /// <returns>Newly created invoice</returns>
        public InvoiceModel Create(
            InvoiceParty from, 
            InvoiceParty @for,
            ICollection<InvoiceLine> items, 
            DateTime? dateOfIssue = null,
            DateTime? paymentDue = null)
        {
            if (@for.Id.Equals(Guid.Parse("0DB60AE3-7BA9-45FC-A789-A5105556C1F3"))) return null; // Not Supported customer
            
            var issueDate = dateOfIssue.HasValue ? dateOfIssue.Value : DateTime.UtcNow;
            var dueDate = paymentDue.HasValue ? paymentDue.Value : DateTime.UtcNow;

            return new InvoiceModel
            {
                For = @for,
                From = from,
                Lines = items,
                DateOfIssue = issueDate,
                PaymentDueOn = dueDate
            };
        }

        /// <summary>
        /// Saves invoice do the database
        /// </summary>
        /// <param name="model"></param>
        public void Save(InvoiceModel model)
        {
            _repository.Add(model);
            _repository.SaveChanges();
        }

        /// <summary>
        /// Easy access to our company data
        /// </summary>
        public static InvoiceParty OurCompany => new()
        {
            Address = "07-780 WBKS Nowogrodzka 17",
            Email = "test@test.pl",
            Id = new Guid("3EF8741D-5D50-4269-A31E-9A149C80F2A4"),
            Name = "Our Componay Sp z o.o.",
            TaxNo = "PL123456789"
        };
    }
}