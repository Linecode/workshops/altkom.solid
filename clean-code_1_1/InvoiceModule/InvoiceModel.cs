using System;
using System.Collections.Generic;
using System.Linq;

namespace clean_code_1_1.InvoiceModule
{
    public class InvoiceModel
    {
        public Guid InvoiceNumber { get; set; }
        public DateTime DateOfIssue { get; set; }
        public DateTime PaymentDueOn { get; set; }

        public InvoiceParty From { get; set; }
        public InvoiceParty For { get; set; }

        public ICollection<InvoiceLine> Lines { get; set; } = new List<InvoiceLine>();

        public InvoiceModel()
        {
            InvoiceNumber = Guid.NewGuid();
        }

        public bool IsValid()
        {
            if (DateOfIssue > PaymentDueOn) return false;
            if (From is null) return false;
            if (For is null) return false;
            if (Lines.Count > 10) return false; // Longer invoices are not supproted because they are not printable on single page.
            
            return true;
        }

        public decimal TotalAmount => Lines.Sum(x => x.Price * x.Quantity);
    }
}