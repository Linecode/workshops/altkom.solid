# Open / Closed Principle

# Zadanie 1.1

- W module CryptoPayment dodaj klasę statyczną `Exchanges`
- W klasie `Exchanges` dodaj prywatne statyczne pole List<ICryptoPayment> _list. Od razu je zainicjalizuj za pomocą `new()`
- W klasie `Exchanges` dodaj publiczne statyczne pole IReadOnlyList<ICryptoPayment> All. Niech te pole zwraca przekonwertowaną listę z poprzedniego pola do listy typu read only.
- W klasie `Exchanges` dodaj publiczną statyczną metodę `void RegisterCryptoExchanges(ICryptoPayment)` której zadaniem będzie dodawanie nowego provider-a crypto do listy.
- Stwórz klasę `CryptoExchangeFinder`
- W konstryktorze klasy CryptoExchangeFinder dodaj paramter `UserContext`, który musisz przypisać do pola prywatnego.
- W klasie `CryptoExchangeFinder` dodaj metodę `ICryptoPayment FindCryptoExchange`
- Przenieś implementację algorytmu wyboru giełdy kryptowalut z klasy `CryptoPayment` do klasy `CryptoExchangeFinder`
- W klasie `CryptoPayment` w konstruktorze utwórz obiekt typu `CryptoExchangeFinder` i dodaj go jako pole prywatne
- Dostosuj implementację metody `BuyTicket` aby wykorzystała obiekt `CryptoExchangeFinder`
- W klasie Program na samym rozpoczęciu metody main, dodaj kod odpowiedzialny za rejestrację obsługiwanych giełd kryptowalut
```csharp
// Application configuration
Exchanges.RegisterCryptoExchange(new FirstCryptoPaymentExchange());
```
- Przetestuj program w różnych konfiguracjach aby zobaczyć dokładnie jak działa