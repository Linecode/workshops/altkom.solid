namespace SRP_1_2
{
    public class ReportData
    {
        public int UserCount { get; set; }
        public int OrdersCount { get; set; }
        public int OrdersActive { get; set; }
        public decimal Turnover { get; set; }
        public TimeRange TimeRange { get; set; }
    }
}