using System;

namespace SRP_1_2
{
    public class TimeRange
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}