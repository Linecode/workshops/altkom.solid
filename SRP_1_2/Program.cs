﻿using System;

namespace SRP_1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var reportGenerator = new ReportGenerator();
            
            Console.WriteLine(reportGenerator.GetReport(ReportGenerator.ReportFormat.String));
            Console.WriteLine(reportGenerator.GetReport(ReportGenerator.ReportFormat.Json));
            Console.WriteLine(reportGenerator.GetReport(ReportGenerator.ReportFormat.Yml));
        }
    }
}