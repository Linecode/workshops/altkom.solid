using System;
using System.Text.Json;
using Bogus;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace SRP_1_2
{
    public class ReportGenerator
    {
        public string GetReport(ReportFormat format)
        {
            var testTimeRange = new Faker<TimeRange>()
                .RuleFor(x => x.Start, f => f.Date.Past())
                .RuleFor(x => x.End, f => f.Date.Recent());
            var testData = new Faker<ReportData>()
                .RuleFor(x => x.UserCount, f => f.Random.Number(100_000, 1_000_000))
                .RuleFor(x => x.OrdersCount, f => f.Random.Number(1_000, 10_000))
                .RuleFor(x => x.OrdersActive, f => f.Random.Number(1, 100))
                .RuleFor(x => x.Turnover, f => f.Random.Decimal(1_000_000m, 10_000_000m))
                .RuleFor(x => x.TimeRange, _ => testTimeRange.Generate());
            var data = testData.Generate();

            switch (format)
            {
                case ReportFormat.String:
                    return $"Report for period: {data.TimeRange.Start} - {data.TimeRange.End}: \n " +
                           $"UsersCount: {data.UserCount} \n " +
                           $"OrdersCount: {data.OrdersCount} \n " +
                           $"OrdersActive: {data.OrdersActive} \n " +
                           $"Turnover: {data.Turnover}";
                
                case ReportFormat.Json:
                    return JsonSerializer.Serialize(data);

                case ReportFormat.Yml:
                {
                    var serializer = new SerializerBuilder()
                        .WithNamingConvention(CamelCaseNamingConvention.Instance)
                        .Build();

                    return serializer.Serialize(data);
                }

                default:
                    throw new NotSupportedException();
            }
        }

        public enum ReportFormat
        {
            String,
            Json,
            Yml
        }
    }
}