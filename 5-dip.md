# Dependency Inversion Principle

## Zadanie 1.1

- W klasach: 
  - FirstCryptoPaymentExchange
  - SecondCryptoPaymentExchange
  - InternalCryptoPaymentExchange
- Zmień implementacje aby klienta giełdy krypto wstrzykiwać przez konstruktor. Pamiętaj aby trzymać się zasady "Programuj do interfejsu a nie do konkretnej implementacji"
- Następnie przejdź do pliku `Program.cs` i dostosuj implementację do nowego api
- Przetestuj czy wszystko działa poprawnie

## Zadanie 1.2

- Do klas FirstCryptoExchangeClient, SecondCryptoExchangeClient, InternalCryptoExchangeClient dodaj interfejsy wskaznikowe.
- Dodaj paczkę nuget-a `Microsoft.Extensions.DependencyInjection`
- W pliku program.cs na samym rozpoczęciu metody main utwórz obiekt klasy `ServiceCollection`;
- Zarejestruj klasy FirstCryptoExchangeClient, SecondCryptoExchangeClient, InternalCryptoExchangeClient w kontenerze DI za pomocą metody `AddTransient<Interfejs, Implementacja>()`
- Zarejestruj klasy FirstCryptoPaymentExchange, SecondCryptoPaymentExchange, InternalCryptoPaymentExchange jako implementacje interfejsu ICryptoPayment
- Dodaj do kontenera DI również obiekt klasy UserContext, tylko tym razem wykrozystaj metodę `AddSingleton(_ => new UserContext)`
- Zarejestruj w kontenerze DI klasę `CryptoExchangeFinder`
- Zmień konstruktor klasy `CryptoExchangeFinder` aby jako drugi parametr przyjmował `IEnumerable<ICryptoPayment> exchanges`
- Zmień implementacje metody `FindCryptoExchange` tak aby korzystała z pobranej listy z kontenera DI
- Skasuj klasę Exchanges (wraz z odwołaniami do niej z pliku Program.cs)
- Dodaj klasę UserNotifier w module CashPayments
- W klasie UserNotifier przez konstruktor pobierz UserContext
- Dodaj metodę `Invoke` w której sprawdzisz czy w obiekcie userContext flaga PhoneNotificationEnabled jest ustawiona na true, jeśli tak to wyświetl komunikat na konsoli
- Zarejestruj klasę UserNotifier w DI
- W klasie TerminalPayment pobierz obiekt klasy UserNotifier przez konstruktor
- Dodaj interfejs IPaymentModelFactory z zdefiniowaną metodą `PaymentModel Create(TicketDetails, PaymentDetails)`
- Zaimplementuj fabryki dla wszystkich metod płatności. Pamiętaj aby jak najwięcej rzeczy pobierać z kontenera DI
- Zarejestruj wszystkie fabryki w kontenerze DI
- Następnie z obiektu services zbuduj obiket ServiceProvider
- Przy pomocy obiektu serviceProvider utwórz obiekt ServiceScope (metoda .CreateScope)
- Dostosuj metodę `GetPaymentOption` tak aby korzystała z service providera-a i tworzyła PaymentModel za pomocą fabryki
