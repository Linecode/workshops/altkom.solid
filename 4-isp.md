# Interface Segregation Principle

## Zadanie 1.1

- Stwórz interfejs `IInternalCryptoClient`, który będzie miał jedną metodę `void Charge(decimal amount)`
- Stwórz interfejs `IExternalCryptoClient`, który będzie rozszerza interfejs `IInternalCryptoClient`. Wewnątrz tego interfejsu dodaj metodę `decimal GetGasFee()`
- Zmień implementację klientów (FirstCryptoExchangeClient, SecondCryptoExchangeClient, InternalCryptoExternalClient) krypto tak aby implementowały interfejsy zgodnę z przeznaczeniem
- Z klasy `InternalCryptoExchangeClient` wykasuj metodę `GetGasFee` jako że nie jest już potrzebna

## Zadanie 1.1+ 

Zastanów się czy interfejs `IExternalCryptoClient` powinien dziedziczyć po `IInternalCryptoClient`. 
Czy widzisz jakieś probelmy takiej implementacji? 
Jak mogłoby wyglądać inne podejście do tej implementacji?